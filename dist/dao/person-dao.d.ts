import { SchemaService, Initializable } from "space-mvc";
import { Person } from "../dto/person";
declare class PersonDao implements Initializable {
    private schemaService;
    private store;
    constructor(schemaService: SchemaService);
    init(): Promise<void>;
    put(key: string, person: Person): Promise<any>;
    get(key: string): Promise<Person>;
    list(offset?: number, limit?: number): Promise<Person[]>;
}
export { PersonDao };
