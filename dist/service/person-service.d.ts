import { PersonDao } from "../dao/person-dao";
import { Person } from "../dto/person";
declare class PersonService {
    private personDao;
    constructor(personDao: PersonDao);
    put(key: string, person: Person): Promise<any>;
    get(key: string): Promise<Person>;
    list(offset?: number, limit?: number): Promise<Person[]>;
}
export { PersonService };
