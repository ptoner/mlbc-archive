import  { ModelView, controller, routeMap, RoutingService, RouteTo } from "space-mvc"


import HomeComponent from '../components/home/home.f7.html'


@controller()
class HomeController {

    constructor(
        private routingService:RoutingService
    ) {}

    @routeMap("/")
    async showIndex(): Promise<ModelView> {
        
        return new ModelView(async () => {
        }, HomeComponent)

    }



}

export { HomeController }
