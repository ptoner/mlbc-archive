import "core-js/stable"
import "regenerator-runtime/runtime"

import SpaceMVC, {IpfsService, SpaceMvcInit } from "space-mvc";

import { container } from "./inversify.config"
import { storeDefinitions } from "./store-definitions"

//Get main app component.
import AppComponent from './components/app.f7.html'

//Import CSS
import './html/css/framework7.bundle.min.css'
import './html/css/framework7-icons.css'
import './html/css/app.css'



export default async () => {

    SpaceMVC.getEventEmitter().on('spacemvc:initFinish', async () => {
        
    })

    let f7Config = {
        el: '#app', // App root element
        id: 'mlbc-archive', // App bundle ID
        name: 'MLBC Archive', // App name
        theme: 'auto', // Automatic theme detection
        component: AppComponent
    }

    let spaceMvcInit:SpaceMvcInit = {
        name: "mlbc-archive",
        displayName: "MLBC Archive",
        container: container,
        f7Config: f7Config,
        // storeDefinitions: storeDefinitions,
        contracts: []
    }


    await SpaceMVC.init(spaceMvcInit)

}
